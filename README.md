# Getting started

- create a .dotfiles folder, which we'll use to track your dotfiles 

```bash
git init --bare $HOME/.dotfiles
```

- create an alias `dotfiles` so you don't need to type it all over again

```bash
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
```

- set git status to hide untracked files

```bash
dotfiles config --local status.showUntrackedFiles no
```

- add the alias to .bashrc (or .zshrc) so you can use it later

```bash
echo "alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.bashrc
```

# Usage 
Now you can use regular git commands such as:

```bash
dotfiles status
dotfiles add .vimrc
dotfiles commit -m "Add vimrc"
dotfiles add .bashrc
dotfiles commit -m "Add bashrc"
dotfiles push
```

# Setup environment in a new computer
Make sure to have git installed, then:

- clone your github repository

```bash
git clone --bare https://github.com/USERNAME/dotfiles.git $HOME/.dotfiles
```

- define the alias in the current shell scope

```
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
```

- checkout the actual content from the git repository to your $HOME

```bash
dotfiles checkout
```

Awesome! You’re done.

refrence: [fwuensche.medium.com](https://fwuensche.medium.com/how-to-manage-your-dotfiles-with-git-f7aeed8adf8b)
