#!/bin/bash

# Make config backup
echo 'Backuping ~/.config/lvim/config.lua to ~/lvim_config.lua.bak...'
cp ~/.config/lvim/config.lua ~/lvim_config.lua.bak

# Uninstall
echo 'Running uninstall script...'
bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/uninstall.sh)

# Install neovim
echo 'Installing neovim...'
# bash <(curl -s https://raw.githubusercontent.com/LunarVim/LunarVim/rolling/utils/installer/install-neovim-from-release)
bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install-neovim-from-release)

# Install lunarvim
echo 'Installing lunarvim...'
# LV_BRANCH=rolling bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/rolling/utils/installer/install.sh)
bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install.sh)

