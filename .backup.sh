#!/bin/bash

# Input gaurd
#
# Example
# . backup.sh mohsen
if [[ $# -ne 1 ]]; then
  echo "Usage: backup.sh <device_name>"
  return
fi

# Initialization
name="/media/`whoami`/$1/`whoami`@`hostname`_backup"
echo -e "Backuping to: $name\n"
mkdir -p "$name"

# Backup list
items[0]="Documents"
items[1]="books"
items[2]="Music"
items[3]="Pictures"
items[4]="5f4dcc3b5aa765d61d8327deb882cf99"
items[5]="todo.md"

# Sync list
cd
for item in "${items[@]}"; do
  echo "Synchronizing $item ..."
  # echo "rsync -ru $item $name"
  rsync -ru "$item" "$name"
  echo -e "Done\n"
done
